package main

import (
	"fmt"
	"log"
	"net"
	"strings"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"

	pb "gitlab.com/motkeg/go-server/grpc/model"
)

const (
	port = ":8080"
)


type server struct{
	savedCustomers []*pb.CustomerRequest
}

// CreateCustomer creates a new Customer
func (s *server) CreateCustomer(ctx context.Context, in *pb.CustomerRequest) (*pb.CustomerResponse, error) {
	s.savedCustomers = append(s.savedCustomers, in)
	return &pb.CustomerResponse{Id: in.Id, Success: true}, nil
}

// GetCustomers returns all customers by given filter
func (s *server) GetCustomers(filter *pb.CustomerFilter, stream pb.Customer_GetCustomersServer) error {
	for _, customer := range s.savedCustomers {
		if filter.Keyword != "" {
			if !strings.Contains(customer.Name, filter.Keyword) {
				continue
			}
		}
		if err := stream.Send(customer); err != nil {
			return err
		}
	}
	return nil
}


//create REST server
func startRESTServer( grpcAddress string) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	mux := runtime.NewServeMux()
	creds, err := credentials.NewClientTLSFromFile("../cert/server.crt", "")
	if err != nil {
	  return fmt.Errorf("could not load TLS certificate: %s", err)
	}
	// Setup the client gRPC options
	opts := []grpc.DialOption{grpc.WithTransportCredentials(creds)}
	// Register ping
	err = pb.RegisterCustomerHandlerFromEndpoint(ctx, mux, grpcAddress, opts)
	if err != nil {
	  return fmt.Errorf("could not register service Ping: %s", err)
	}
	log.Printf("starting HTTP/1.1 REST server on %s", grpcAddress)
	http.ListenAndServe(grpcAddress, mux)
	return nil
  }

func main()  {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	 // Create the TLS credentials
	 creds, err := credentials.NewServerTLSFromFile("../cert/server.crt", "../cert/server.key")
	 if err != nil {
	   log.Fatalf("could not load TLS keys: %s", err)
	 }
	 // Create an array of gRPC options with the credentials
	 opts := []grpc.ServerOption{grpc.Creds(creds)}

	// Creates a new gRPC server
	s := grpc.NewServer(opts[0])
	pb.RegisterCustomerServer(s, &server{})
	fmt.Printf("grpc: on%s " , port)
	s.Serve(lis)

	 // fire the REST server in a goroutine
	 go func() {
		err := startRESTServer("loaclhost"+port)
		if err != nil {
		  log.Fatalf("failed to start gRPC server (REST): %s", err)
		}
	  }()
}