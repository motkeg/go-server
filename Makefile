SERVER_OUT := "bin/server"
CLIENT_OUT := "bin/client"
PKG := "gitlab.com/motkeg/go-server/grpc"
SERVER_PKG_BUILD := "${PKG}/server"
CLIENT_PKG_BUILD := "${PKG}/client"
PROTO = "grpc/model"
.PHONY: all api server client 
all: server client

api: ## compile .proto file
  @grpc/model/protobub.cmd

dep: ## Get the dependencies
  @go get -v -d ./...

server: api ## Build the binary file for server
  @go build -i -v -o $(SERVER_OUT) $(SERVER_PKG_BUILD)

client: api ## Build the binary file for client
  @go build -i -v -o $(CLIENT_OUT) $(CLIENT_PKG_BUILD)

clean: ## Remove previous builds
  @rm $(SERVER_OUT) $(CLIENT_OUT) $(API_OUT) $(API_REST_OUT)  